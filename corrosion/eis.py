import numpy as np
import matplotlib
import matplotlib.pyplot as plt


class Conexion():

    def paral(self, *args):
        '''Calcula la impedancia de un elemento circuital cualquiera'''
        pa = 0
        for arg in args:
            pa += 1 / arg
        return 1/pa

    def serie(self, *args):
        '''Calcula la impedancia de un elemento circuital cualquiera'''
        pa = 0
        for arg in args:
            pa += arg
        return pa


class Circuito(Conexion):

    moduloZ = 0
    theta = 0
    simulacion = 0

    def __init__(self, cdc, ini, fin, ppd):
        # CDC = CIRCUIT DESCRIPTION CODE, propuesto por Bouckamp
        self.cdc = self.elem_CDC(cdc)[0]
        self.sweep = self.frecuencias(ini, fin, ppd)
        self.elementos = self.elem_CDC(cdc)[1]
        self.funcion = self.elem_CDC(cdc)[2]

    def frecuencias(self, f_inicio, f_final, pts_decada=10):
        '''
        Descripción
        -----------
        Generador de rango de barrido, frecuencias log espaciadas

        Parámetros
        ----------
        f_inicio = Frecuencia de inicio [Hz]
        f_final = Frecuencia final [Hz]
        pts_decada = Puntos por década, por defecto 10

        Salida
        ------
        f_rango = rango de frecuencias [Hz]
        '''
        f_decadas = np.log10(f_inicio) - np.log10(f_final)
        f_rango = np.logspace(np.log10(f_inicio), np.log10(f_final), num=int(
            np.around(pts_decada*f_decadas)), endpoint=True)
        return f_rango

    def R(self, r):
        '''
        Descripción
        -----------
        Calcula la impedancia de un elemento resistivo. 

        Parámetros:
        ----------
        r = resistencia [ohms]

        Salida
        ------
        imp = retorna un impedancia compleja (con parte imaginaria nula) [ohms]
        '''
        imp = np.complex_(np.zeros(len(self.sweep)))
        imp.imag = 0
        imp.real = r
        return imp

    def C(self, c):
        '''
        Descripción
        -----------
        Calcula la reactancia de un elemento capacitivo. 

        Parámetros:
        ----------
        c = capacitancia [Faradios]

        Salida
        ------
        imp = retorna un impedancia compleja (con parte real nula) [ohms]
        '''
        imp = np.complex_(np.zeros(len(self.sweep)))
        imp.real = 0
        imp.imag = (-1/(self.sweep * c))

        return imp

    def L(self, l):
        '''
        Descripción
        -----------
        Calcula la impedancia de un elemento inductivo. 

        Parámetros:
        ----------
        l = inductancia [Henry]

        Salida
        ------
        imp = retorna un impedancia compleja (con parte real nula) [ohms]
        '''
        imp = np.complex_(np.zeros(len(self.sweep)))
        imp.imag = self.sweep * l
        return imp

    def Q(self, Q, n):
        '''
        Descripción
        -----------
        Calcula la impedancia de un elemento de fase constante. 

        Parámetros:
        ----------
        Q = ..... [....]
        n = 90/n......

        Salida
        ------
        imp = retorna un impedancia compleja (con parte real nula) [ohms]
        '''
        d = np.complex_(np.zeros(len(self.sweep)))
        d.imag = 1
        return 1 / (Q * (d * self.sweep)**n)

    def W(self, Y0):
        d = np.complex_(np.zeros(len(self.sweep)))
        d.imag = 1
        return 1/(np.sqrt(d * 2 * np.pi * self.sweep) * Y0)

    def T(self, Y0, B):
        """
        Impedancia FSW - Finite length diffusion elements.  The FSW (Finite Space Warburg) T
        """
        d = np.complex_(np.zeros(len(self.sweep)))
        d.imag = 1
        omega = 2 * np.pi * self.sweep
        argu = B * np.sqrt(d * omega)
        return np.cosh(argu)/np.sinh(argu)/Y0 * np.sqrt(d * omega)

    def O(self, Y0, B):
        """
        Impedancia FLW - Finite length diffusion elements.  The FLW (Finite Length Warburg) O
        Entrada:
        -------
        Y0: 
        B: espesor película / sqrt(coeficiente de difusión)
        """
        d = np.complex_(np.zeros(len(self.sweep)))
        d.imag = 1
        omega = 2 * np.pi * self.sweep
        argu = B * np.sqrt(d * omega)
        return np.tanh(argu)/Y0 * np.sqrt(d * omega)

    def G(self, Y0, k):
        """
        Impedancia Gerisher G.
        Entrada:
        -------
        Y0: 
        k:
        """
        d = np.complex_(np.zeros(len(self.sweep)))
        # d.imag = 1
        omega = 2 * np.pi * self.sweep
        return (k + d * omega)**(-0.5)/Y0

# # Impedancia Gerisher G.
# zg <- function(f, Yo, k) {
#     omega <- 2 * pi * f
#     (k + j * omega)^(-0.5)/Yo
# }

# # Impedancia Gerisher Fractal F.
# zf <- function(f, Yo, k, alfa) {
#     omega <- 2 * pi * f
#     (k + j * omega)^(alfa)/Yo
# }

    def elem_CDC(self, cdc):
        # Genero una lista con el string CDC
        # Y paso los elementos a mayúsculas
        lista = list(cdc.upper())
        lista_elem = []
        funcion = ""
        # Inicializo a 1 todos los contadores, para generar subíndices de
        # cada elemento.
        rs = cs = qs = ls = ws = ts = os = gs = 1
        for i in range(len(lista)):
            if lista[i] == "R":
                lista[i] = "R"+str(rs)
                lista_elem.append(lista[i])
                funcion = funcion + \
                    "self.R(self.elementos[\'" + lista[i] + "\']) ,"
                rs = rs + 1
            elif lista[i] == "C":
                lista[i] = "C"+str(cs)
                lista_elem.append(lista[i])
                funcion = funcion + \
                    "self.C(self.elementos[\'" + lista[i] + "\']),"
                cs = cs + 1
            elif lista[i] == "L":
                lista[i] = "L"+str(ls)
                lista_elem.append(lista[i])
                funcion = funcion + \
                    "self.L(self.elementos[\'" + lista[i] + "\']),"
                ls = ls + 1
            elif lista[i] == "Q":
                lista[i] = "Q"+str(qs)
                lista_elem.append(lista[i])
                lista_elem.append("n"+str(qs))
                funcion = funcion + \
                    "self.Q(self.elementos[\'" + lista[i] + \
                    "\'],self.elementos[\'n" + str(qs) + "\']),"
                qs = qs + 1
            elif lista[i] == "W":
                lista[i] = "W"+str(ws)
                lista_elem.append(lista[i])
                funcion = funcion + \
                    "self.W(self.elementos[\'" + lista[i] + "\']),"
                ws = ws + 1
            elif lista[i] == "T":
                lista[i] = "T"+str(ts)
                lista_elem.append(lista[i])
                lista_elem.append("b"+str(ts))
                funcion = funcion + \
                    "self.T(self.elementos[\'" + lista[i] + \
                    "\'],self.elementos[\'b" + str(ts) + "\']),"

            elif lista[i] == "O":
                lista[i] = "O"+str(os)
                lista_elem.append(lista[i])
                lista_elem.append("b"+str(os))
                funcion = funcion + \
                    "self.O(self.elementos[\'" + lista[i] + \
                    "\'],self.elementos[\'b" + str(ts) + "\']),"

            elif lista[i] == "G":
                lista[i] = "G"+str(gs)
                lista_elem.append(lista[i])
                lista_elem.append("k"+str(gs))
                funcion = funcion + \
                    "self.G(self.elementos[\'" + lista[i] + \
                    "\'],self.elementos[\'k" + str(gs) + "\']),"

            else:
                if lista[i] == "(":
                    funcion = funcion + " self.paral("
                elif lista[i] == "[":
                    funcion = funcion + "self.serie("
                elif lista[i] == ")":
                    funcion = funcion + ")"
                elif lista[i] == "]":
                    funcion = funcion + ")"

        funcion = funcion.replace("),)", "))")  # corrige patron ),) por ))
        funcion = funcion.replace(") self", "), self")
        funcion = "self.serie(" + funcion + ")"
        self.cdc = ' '.join(lista)
        dicc = dict.fromkeys(lista_elem, 0.)
        return(' '.join(lista), dicc, funcion)

    def simula(self):

        sim = eval(self.funcion)
        self.moduloZ = np.sqrt(sim.real ** 2 + sim.imag ** 2)
        self.theta = np.arctan((sim.imag)/sim.real)*(180/np.pi)
        self.simulacion = sim
        
        return(sim)

    def nyquist(self):

        fig, ax = plt.subplots()
        ax.plot(self.simulacion.real, -self.simulacion.imag, 'bo')
        ax.axis('square')
        ax.set(xlabel=r'$Z´ (\Omega . cm^2)$', ylabel=r'$-Z´´ (\Omega . cm^2)$',
               title='Diagrama Nyquist')
        ax.grid(True)
        plt.show()

    def bode(self):

        # Create two subplots sharing y axis
        fig, (ax1, ax2) = plt.subplots(2, sharex=True)
        ax1.grid(True)
        ax1.set_xscale('log')
        ax1.plot(self.sweep, self.moduloZ, 'k.')
        ax1.set(title='Diagramas de Bode |Z| y Fase',
                ylabel=r'$|Z|  (\Omega . cm^2)$')
        ax2.grid(True)
        ax2.plot(self.sweep, self.theta, 'r.')
        ax2.set(xlabel='Frecuencia (Hz)', ylabel='Fase (°)')
        r'$Z´ (\Omega . cm^2)$'
        plt.show()

    def imag_real(self):

        # Create two subplots sharing y axis
        fig, (ax1, ax2) = plt.subplots(2, sharex=True)
        ax1.grid(True)
        ax1.set_xscale('log')
        ax1.plot(self.sweep, self.simulacion.real, 'k.')
        ax1.set(title='Diagramas de Bode Real e Imag',
                ylabel=r'$Real  (\Omega . cm^2)$')
        ax2.grid(True)
        ax2.plot(self.sweep, self.simulacion.imag, 'r.')
        ax2.set(xlabel='Frecuencia (Hz)', ylabel=r'$Imag  (\Omega . cm^2)$')
        plt.show()

if __name__ == "__main__":
    
    # Circuito Prueba
    my = Circuito("(qr)(lr)", 50000, 0.05, 7)

    # Qué elementos tengo que ajustar??. Están todos inicializados  a cero
    print("Imprime el CDC de Bouckamp: \n")
    print(my.cdc)

    print(my.sweep)
    print(my.elementos)
    print(my.funcion)
    # Accedo a cada uno de ellos por su nombre de la siguiente manera:
    my.elementos['L1'] = 0.5
    my.elementos['R1'] = 14325
    my.elementos['Q1'] = 1E-6
    my.elementos['n1'] = 0.75
    my.elementos['R2'] = 123434

    print(my.elementos)


    # Con los valores asignados ya puedo simular en el rango de frecuencias que establecí al crear el objeto my
    # me devuelve un vector de ns complejos

    simulacion = my.simula()
    my.bode()
    my.nyquist()
    my.imag_real()
    # print(simulacion)
