import pandas as pd
import numpy as np

class Measurement:
    
    def __init__(self, file):
        file_handler = open(file, "r", encoding="ISO-8859-1")
        data = file_handler.readlines()
        file_handler.close()
        
        if "EXPLAIN" in data[0]:
            self.brand = "Gamry"
            self.gamry(data)
            
        elif "ZPlot" in data[0]:
            self.brand = "Zplot"
            self.zplot(data)

        elif "CORRW" in data[0]:
            self.brand = "CorrWare"
            self.corrware(data)
            
          
    def gamry(self, data):
        #print("Soy Gamry !!!")
        if  "EISPOT" in data[1]:
            self.experiment = "Potentiostatic Impedance"
            table = False
            flag = False
            zdata = False
            for each_line in data:
                if "#" in each_line:
                    continue
                if "DATE" in each_line:
                    self.date = ".".join(each_line.rstrip().split("\t")[2:3])
                elif "TIME" in each_line:
                    self.time = ".".join(each_line.rstrip().split("\t")[2:3])
                elif "VDC" in each_line:
                    self.vdc = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "FREQINIT" in each_line:
                    self.freqinit = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "FREQFINAL" in each_line:
                    self.freqfinal = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "PTSPERDEC" in each_line:
                    self.ptsperdec = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "VAC" in each_line:
                    self.vac = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "AREA" in each_line:
                    self.area = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "OCVCURVE" in each_line or table == True:
                    if table:
                        if cont <= puntos:
                            if cont == 0:
                                header =  each_line.rstrip().split("\t")[2:]
                                tabla = np.ndarray(shape = (1,7), dtype = float, buffer = None)

                            else:
                                tabla = np.vstack([tabla, each_line.rstrip().split("\t")[1:]])
                                
                            cont += 1
                        else:
                            table = False
                            tabla = np.delete(tabla, 0 ,0)
                            tabla = np.delete(tabla, 0 ,1)    #: borra primera col
                            tabla = np.delete(tabla, 4 ,1)    #: borra primera col
                            tabla = np.asarray(tabla, dtype=np.float64)
                            del header[4]
                            ocvcu = pd.DataFrame(tabla, columns=header)
                            self.ocvcurve = ocvcu
                        
                    else:
                        puntos = int(".".join(each_line.rstrip().split("\t")[2:3]))
                        table = True
                        cont = 0
                    if "EOC" in each_line:
                        self.eoc = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))

                elif "ZCURVE" in each_line:
                    flag = True
                    zdata = True
                                   
                
                elif flag or zdata:
                    
                    if flag:
                        header =  each_line.rstrip().split("\t")[2:]
                        tabla = np.ndarray(shape = (1,11), dtype = float, buffer = None)
                        flag = False
                        cont = 0
                        continue
                    if zdata:
                        tabla = np.vstack([tabla, each_line.rstrip().split("\t")[1:]])
                        cont += 1
            
            tabla = np.delete(tabla, 0 ,0)    #: borra primera fila
            tabla = np.delete(tabla, 0 ,1)    #: borra primera col
            tabla = np.asarray(tabla, dtype=np.float64)
            zcu = pd.DataFrame(tabla, columns=header)
            self.zcurve = zcu            

                        
        elif "CORPOT" in data[1]:
            self.experiment = "Corrosion Potential"
        elif "CV" in data[1]:
            self.experiment = "Cyclic voltammetry"
        elif "CHRONOA" in data[1]:
            self.experiment = "Chronoamperometry"
            
            
    def zplot(self, data):
        print("Soy Zplot !!!")
        print(data)
    

    def corrware(self, data):
        print("Soy CorrWare !!!")
        if  "Potential Square-Wave" in data[2]:
            self.experiment = "Potential Square-Wave"
            table = False
            flag = False
            zdata = False
            for each_line in data:
                
                if "Open Circuit Potential (V)" in linea:
                    self.ocp = float(linea.rstrip().split(":")[1])
                elif "Data Points:" in linea:
                    puntos = int(linea.rstrip().split(":")[1])
                    datos = lineas[-puntos:]

                if "#" in each_line:
                    continue
                if "DATE" in each_line:
                    self.date = ".".join(each_line.rstrip().split("\t")[2:3])
                elif "TIME" in each_line:
                    self.time = ".".join(each_line.rstrip().split("\t")[2:3])
                elif "VDC" in each_line:
                    self.vdc = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "FREQINIT" in each_line:
                    self.freqinit = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "FREQFINAL" in each_line:
                    self.freqfinal = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "PTSPERDEC" in each_line:
                    self.ptsperdec = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "VAC" in each_line:
                    self.vac = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "AREA" in each_line:
                    self.area = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "OCVCURVE" in each_line or table == True:
                    if table:
                        if cont <= puntos:
                            if cont == 0:
                                header =  each_line.rstrip().split("\t")[2:]
                                tabla = np.ndarray(shape = (1,7), dtype = float, buffer = None)

                            else:
                                tabla = np.vstack([tabla, each_line.rstrip().split("\t")[1:]])
                                
                            cont += 1
                        else:
                            table = False
                            tabla = np.delete(tabla, 0 ,0)
                            tabla = np.delete(tabla, 0 ,1)    #: borra primera col
                            tabla = np.delete(tabla, 4 ,1)    #: borra primera col
                            tabla = np.asarray(tabla, dtype=np.float64)
                            del header[4]
                            ocvcu = pd.DataFrame(tabla, columns=header)
                            self.ocvcurve = ocvcu
                        
                    else:
                        puntos = int(".".join(each_line.rstrip().split("\t")[2:3]))
                        table = True
                        cont = 0
                    if "EOC" in each_line:
                        self.eoc = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))

                elif "ZCURVE" in each_line:
                    flag = True
                    zdata = True
                                   
                
                elif flag or zdata:
                    
                    if flag:
                        header =  each_line.rstrip().split("\t")[2:]
                        tabla = np.ndarray(shape = (1,11), dtype = float, buffer = None)
                        flag = False
                        cont = 0
                        continue
                    if zdata:
                        tabla = np.vstack([tabla, each_line.rstrip().split("\t")[1:]])
                        cont += 1
            
            tabla = np.delete(tabla, 0 ,0)    #: borra primera fila
            tabla = np.delete(tabla, 0 ,1)    #: borra primera col
            tabla = np.asarray(tabla, dtype=np.float64)
            zcu = pd.DataFrame(tabla, columns=header)
            self.zcurve = zcu            

                        
        elif "CORPOT" in data[1]:
            self.experiment = "Corrosion Potential"
        elif "CV" in data[1]:
            self.experiment = "Cyclic voltammetry"
        elif "CHRONOA" in data[1]:
            self.experiment = "Chronoamperometry"