import pandas as pd
import numpy as np

class Measurement:

    def __init__(self, file):
        self.experiment =""
        self.file = file
        file_handler = open(file, "r", encoding="ISO-8859-1")
        data = file_handler.readlines()
        file_handler.close()
        
        if "EXPLAIN" in data[0]:
            self.brand = "Gamry"
            self.gamry(data)
            
        elif "ZPlot" in data[0]:
            self.brand = "Zplot"
            self.zplot(data)

        elif "CORRW" in data[0]:
            self.brand = "CorrWare"
            self.corrware(data)
            
          
    def gamry(self, data):
        print("Soy Gamry !!!")
        if  "EISPOT" in data[1]:
            self.experiment = "Potentiostatic Impedance"
            print("Experimento: ", self.experiment)
            table = False
            flag = False
            zdata = False
            for each_line in data:
                if "#" in each_line:
                    continue
                if "DATE" in each_line:
                    self.date = ".".join(each_line.rstrip().split("\t")[2:3])
                elif "TIME" in each_line:
                    self.time = ".".join(each_line.rstrip().split("\t")[2:3])
                elif "VDC" in each_line:
                    self.vdc = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "FREQINIT" in each_line:
                    self.freqinit = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "FREQFINAL" in each_line:
                    self.freqfinal = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "PTSPERDEC" in each_line:
                    self.ptsperdec = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "VAC" in each_line:
                    self.vac = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "AREA" in each_line:
                    self.area = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "OCVCURVE" in each_line or table == True:
                    if table:
                        if cont <= puntos:
                            if cont == 0:
                                header =  each_line.rstrip().split("\t")[2:]
                                tabla = np.ndarray(shape = (1,7), dtype = float, buffer = None)

                            else:
                                tabla = np.vstack([tabla, each_line.rstrip().replace(",", ".").split("\t")[1:]])
                                
                            cont += 1
                        else:
                            table = False
                            tabla = np.delete(tabla, 0 ,0)
                            tabla = np.delete(tabla, 0 ,1)    #: borra primera col
                            tabla = np.delete(tabla, 4 ,1)    #: borra primera col
                            tabla = np.asarray(tabla, dtype=np.float64)
                            del header[4]
                            ocvcu = pd.DataFrame(tabla, columns=header)
                            self.ocvcurve = ocvcu
                        
                    else:
                        puntos = int(".".join(each_line.rstrip().split("\t")[2:3]))
                        table = True
                        cont = 0
                    if "EOC" in each_line:
                        self.ocp = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))

                elif "ZCURVE" in each_line:
                    flag = True
                    zdata = True
                                   
                
                elif flag or zdata:
                    
                    if flag:
                        header =  each_line.rstrip().split("\t")[2:]
                        tabla = np.ndarray(shape = (1,11), dtype = float, buffer = None)
                        flag = False
                        cont = 0
                        continue
                    if zdata:
                        tabla = np.vstack([tabla, each_line.rstrip().replace(",", ".").split("\t")[1:]])
                        cont += 1
            
            tabla = np.delete(tabla, 0 ,0)    #: borra primera fila
            tabla = np.delete(tabla, 0 ,1)    #: borra primera col
            tabla = np.asarray(tabla, dtype=np.float64)
            zcu = pd.DataFrame(tabla, columns=header)
            self.data = zcu            

                        
        elif "CORPOT" in data[1]:
            self.experiment = "Corrosion Potential"
        elif "CV" in data[1]:
            self.experiment = "Cyclic voltammetry"
        elif "REPEATING_CHRONOA" in data[1]:
            self.experiment = "Chronoamperometry Scan"
            print("Experimento: ", self.experiment)
            table = False
            flag = False
            zdata = False
            skip = 0
            for each_line in data:
                skip = skip + 1
                if "#" in each_line:
                    continue
                if "DATE" in each_line:
                    self.date = ".".join(each_line.rstrip().split("\t")[2:3])
                elif "TIME" in each_line:
                    self.time = ".".join(each_line.rstrip().split("\t")[2:3])
                elif "VSTEP1" in each_line:
                    self.vstep1 = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "TSTEP1" in each_line:
                    self.tstep1 = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "VSTEP2" in each_line:
                    self.vstep2 = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "TSTEP2" in each_line:
                    self.tstep2 = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "AREA" in each_line:
                    self.area = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "SAMPLETIME" in each_line:
                    self.sample = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))
                elif "OCVCURVE" in each_line or table == True:
                    if table:
                        if cont <= puntos:
                            if cont == 0:
                                header =  each_line.rstrip().split("\t")[2:]
                                tabla = np.ndarray(shape = (1,7), dtype = float, buffer = None)

                            else:
                                tabla = np.vstack([tabla, each_line.rstrip().replace(",", ".").split("\t")[1:]])
                                
                            cont += 1
                        else:
                            table = False
                            tabla = np.delete(tabla, 0 ,0)
                            tabla = np.delete(tabla, 0 ,1)    #: borra primera col
                            tabla = np.delete(tabla, 4 ,1)    #: borra primera col
                            tabla = np.asarray(tabla, dtype=np.float64)
                            del header[4]
                            ocvcu = pd.DataFrame(tabla, columns=header)
                            self.ocvcurve = ocvcu
                        
                    else:
                        puntos = int(".".join(each_line.rstrip().split("\t")[2:3]))
                        table = True
                        cont = 0
                    if "EOC" in each_line:
                        self.ocp = float(".".join(each_line.rstrip().split("\t")[2:3]).replace(",", "."))

                elif "CURVE" in each_line:
                    skip = skip + 2
                    header = ("un", "Pt", "T", "V", "I", "Vu",  "Sig", "Ach", "IERange", "Over", "Temp")
                    datos = pd.read_csv(self.file, sep='\t', skiprows = skip, names=header,decimal=",")
                    datos.drop(['un', 'Pt', 'IERange', 'Over'], axis = 'columns', inplace=True)
                    
                    self.data = datos
                
                        
            
    def zplot(self, data):
        print("Soy Zplot !!!")
        print(data)
    

    def corrware(self, data):
        print("Soy CorrWare !!!")
        aux = False
        measure_rate = 1
        oversample = 1
        if  "Potential Square-Wave" in data[2]:
            self.experiment = "Potential Square-Wave"
            print("Experimento: ", self.experiment)
            flag = False
            zdata = False
            for each_line in data:

                if "Open Circuit Potential (V):" in each_line :
                    self.ocp = float(each_line.rstrip().split(":")[1])

                elif "Measure Rate #1:" in each_line :
                    measure_rate = float(each_line.rstrip().split(":")[1])
                    
                elif "Oversample Rate:" in each_line :
                    oversample = float(each_line.rstrip().split(":")[1])
                    
                elif "Aux Channels:" in each_line:
                    flag = True
                    aux = True
                
                elif "End Comments" in each_line:
                    if flag == False:
                    	zdata = True
                    	flag = True
                    zdata = True

                elif flag or zdata:
                    
                    if flag:
                        if aux == True:
                        	header =  ("V","I","T","Aux")
                        	cols = 4
                        else:
                        	header =  ("V","I","T")
                        	cols = 3
                        
                        tabla = np.ndarray(shape = (1,cols), dtype = float, buffer = None)
                        flag = False
                        cont = 0
                    if zdata:
                        tabla = np.vstack([tabla, each_line.rstrip().split("\t")])
                        cont += 1
            
            tabla = np.delete(tabla, 0 ,0)    #: borra primera fila
            # tabla = np.delete(tabla, 0 ,1)    #: borra primera col
            tabla = np.asarray(tabla, dtype=np.float64)
            zcu = pd.DataFrame(tabla, columns=header)
            self.sample_rate = measure_rate / oversample
            self.data = zcu