import matplotlib.pyplot as plt

def nyquist(real, imag):
    """
    Descripción
    -----------
    Grafica diagrama de Nyquista. 
    
    Parámetros:
    ----------
    real = parte real de la impedancia  [ohm.cm2]
    imag = parte imaginaria de la impedancia [ohm.cm2]
    
    Salida
    ------
    gráfico
    """

    fig, ax = plt.subplots()
    ax.plot(real, -imag, 'o',  ms=5, alpha=0.7, mfc='orange')
    ax.axis('square')
    ax.set(xlabel=r'$Z´ (\Omega . cm^2)$', ylabel=r'$-Z´´ (\Omega . cm^2)$',
    title='Diagrama Nyquist')
    ax.grid(True)
    plt.figure(figsize=(20,20))
    plt.show()