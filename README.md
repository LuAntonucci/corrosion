## ¿Qué es Corrosion?
   
   Agregá aquí la descripción de tu proyecto

## ¿Cómo colaborar con el proyecto?

1. 
```bash
# Asegurarse de tener instalado python3, pip y virtualenv, sino:
sudo apt install python3-pip python3-virtualenv

# Crear entorno virtual para enjaular diferentes versiones de python y paquetes.
python3 -m venv ~/virtual/corrosion

# Para Activar entorno virtual: 
source ~/virtual/corrosion/bin/activate

# Para desactivarlo tipee: 
deactivate
```

2. Haz un fork del proyecto a tu cuenta Gitlab:

   https://gitlab.com/mendivilg/corrosion/-/forks/new

3. 
```bash

# Clonar el repo a su computadora (reemplace su_usuario_gitlab por el suyo).
git clone git@gitlab.com:su_usuario_gitlab/corrosion.git

# Ingresar a la carpeta clonada.
cd corrosion

# Instalar paquetes requeridos por el proyecto.
python -m pip install -r requirements.txt

# Agregar la rama remota del proyecto principal.
git remote add upstream git@gitlab.com:mendivilg/corrosion.git

# Crear una rama donde trabajar en su aporte y nombrela de manera descriptiva (ej. readme_feature).
git checkout -b rama_con_mi_aporte

# En este punto haga su aporte, pruébelo, y luego haga stage y commit.
git add .
git commit -m "Descripción resumida y precisa del aporte"

# Actualizar su rama a la última versión del Proyecto Principal. 
git pull upstream rama_con_mi_aporte

# Subir su rama a su proyecto forkeado (origin) en Gitlab.
git push origin rama_con_mi_aporte

```

3. En su cuenta Gitlab cree un Pull Request (PR), a partir de la rama *rama_con_mi_aporte*.
